package Model;

import javax.persistence.*;

/**
 * An implementation of Model.MasterDao
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-17
 */
public class MasterDao<T> implements Dao<T> {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /**
     * this is the base entity manager that manages entities (obvious)
     */
    protected EntityManager entityManager;

    /**
     * object for the current transaction. should be initialized in every transaction.
     */
    protected EntityTransaction entityTransaction = null;

    /**
     * this is the class that will be searched on the database
     */
    protected Class<T> typeClass;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /**
     * this object is able to create entity managers which are then used to make database transactions
     */
    @PersistenceContext
    EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("org.hibernate.tutorial.jpa");

    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * creates a new dao-object and initializes several objects for the database connection
     */
    public MasterDao(Class<T> typeClass){
        this.entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        this.typeClass = typeClass;
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public void persist(final T entity) {
        doInTransaction(() -> entityManager.persist(entity));
    }

    public void remove(final T entity) {
        doInTransaction(() -> entityManager.remove(entity));
    }

    public T findById(final long id) {

        T result = null;

        try {

            // get a transaction
            entityTransaction = entityManager.getTransaction();

            //start the transaction
            entityTransaction.begin();

            //save the result
            result = entityManager.find(typeClass, id);

        } catch (IllegalStateException | IllegalArgumentException e ){
            // TODO: log the error with our logger
            System.err.println(e);

        }

        // commit the changes and close the entity manager
        entityTransaction.commit();

        // return result or null, if nothing was found
        return result;
    }

    /**
     * this method wraps every action in a seperate transaction to ensure database safety.
     * the functional interface is used to be able to use lambda-expressions
     * @param masterTransaction Implementation of the Model.MasterTransaction class
     */
    protected void doInTransaction(MasterTransaction masterTransaction){

        try {

            // get a transaction
            entityTransaction = entityManager.getTransaction();

            //start the transaction
            entityTransaction.begin();


            // now do the action that was implemented
            masterTransaction.doInTransaction();

        } catch (Exception e){

            // rollback any changes for safety
            entityTransaction.rollback();

            // TODO: log the error with our logger
            System.err.println(e);


        }

        // commit the changes
        entityTransaction.commit();


    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

