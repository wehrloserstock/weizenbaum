package Model;

/**
 * This class is only present for providing features to the underlying models.
 * it should not be used in views or controllers as a point of reference.
 * rather use the models appropriate for your use instead (like MovieModel for getting movies)
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-19
 */
public abstract class MasterModel {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */
    static {
        if (checkInternetConnection())
            apiConnection = checkAPIConnection();
        else
            apiConnection = false;

    }


    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    /**
     * value determining if the internet connection (especially to the tmdb database could be established or not)
     */
    protected static boolean apiConnection = false;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * checks the internet connection
     * @return boolean
     */
    private static boolean checkAPIConnection(){
        // check the internet connection
        return false;
    }

    /**
     * checks the internet connection
     * @return boolean
     */
    private static boolean checkInternetConnection(){
        return false;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */



}

