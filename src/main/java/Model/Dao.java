package Model;


/**
 * An implementation of Model.Dao
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-17
 */
public interface Dao<T> {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * persists (saves) the given entity to the configured database.
     * @param entity T
     */
    void persist(T entity);

    /**
     * searches the given entity on the database and removes it
     * @param entity
     */
    void remove(T entity);

    /**
     * finds the entity on the database with the given id and returns the object
     * @param id long - id of the searched object
     * @return T result or null if nothing was found
     */
    T findById(long id);

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

