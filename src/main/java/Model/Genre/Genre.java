package Model.Genre;

public enum Genre{
    ACTION("ACTION"), ADVENTURE("ADVENTURE"), DEFAULT("NOT_FOUND");
    
    private String value;
    Genre(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static Genre getEnum(String value) {
        for(Genre v : values())
            if(v.getValue().equalsIgnoreCase(value)) return v;
        return DEFAULT;
        //TODO log here
    }
}

