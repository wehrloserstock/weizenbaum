package Model.User;

/**
 * An implementation of UserModel
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-31
 */
public interface UserModel {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

