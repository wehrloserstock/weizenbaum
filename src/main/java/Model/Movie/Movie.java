package Model.Movie;

import Model.Genre.Genre;

import java.net.URL;
import java.sql.Date;
import java.util.List;

/**
 * An implementation of Movie
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-17
 */
 public interface Movie {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    List<Movie> getMovies(String search);

    List<Movie> getSimilarMovies();

    void setSimilarMovies(List<Movie> similarMovies);

    /**
     * @return the videos
     */
     List<URL> getVideos();

    /**
     * @param videos the videos to set
     */
     void setVideos(List<URL> videos);

    /**
     * @return the runtime
     */
     int getRuntime();

    /**
     * @param runtime the runtime to set
     */
     void setRuntime(int runtime);

    /**
     * @return the date
     */
     Date getDate();

    /**
     * @param date the date to set
     */
     void setDate(Date date);

    /**
     * @return the rating
     */
     int getRating();

    /**
     * @param rating the rating to set
     */
     void setRating(int rating);

    /**
     * @return the title
     */
     String getTitle();

    /**
     * @param title the title to set
     */
     void setTitle(String title);

    /**
     * @return the posterURL
     */
     URL getPosterURL();

    /**
     * @param posterURL the posterURL to set
     */
     void setPosterURL(URL posterURL);

    /**
     * @return the movieImages
     */
     List<URL> getMovieImages();

    /**
     * @param movieImages the movieImages to set
     */
     void setMovieImages(List<URL> movieImages);

    /**
     * @return the lists
     */
     List<List<Movie>> getLists();

    /**
     * @param lists the lists to set
     */
     void setLists(List<List<Movie>> lists);

    /**
     * @return the description
     */
     String getDescription();

    /**
     * @param description the description to set
     */
     void setDescription(String description);

    /**
     * @return the genres
     */
     List<Genre> getGenres();

    /**
     * @param genres the genres to set
     */
     void setGenres(List<Genre> genres);

    /**
     * @return the backdropUrl
     */
     URL getBackdropUrl();

    /**
     * @param backdropUrl the backdropUrl to set
     */
     void setBackdropUrl(URL backdropUrl);

    /**
     * @return the adult
     */
     boolean isAdult();

    /**
     * @param adult the adult to set
     */
     void setAdult(boolean adult);

}

