package Model.Movie;

import Model.Genre.Genre;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MovieImpl implements Movie{

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    private boolean adult;
    private URL backdropUrl;
    private String description;
    private List<Genre> genres;
    private List<List<Movie>> lists;
    private List<URL> movieImages;
    private URL posterURL;
    private String title;
    private int rating;
    private Date date;
    private int runtime; // In seconds? Float? Date?
    private List<URL> videos;
    private List<Movie> similarMovies;

    /*
     * ---------------------------------------- Constants
     * -----------------------------------------------------------
     */

    /*
     * ---------------------------------------- Constructors
     * --------------------------------------------------------
     */
    public MovieImpl(){
        this.adult = false;
        this.backdropUrl = null;
        this.description = "";
        this.genres = new ArrayList<Genre>();
        this.lists = new  ArrayList<List<Movie>>();
        this.movieImages = new ArrayList<URL>();
        this.posterURL = null;
        this.title = "";
        this.rating = 0;
        this.date = null;
        this.runtime = 0;
        this.videos = new ArrayList<URL>();
        this.similarMovies = new LinkedList<Movie>();
    }
    /*
     * ---------------------------------------- Methods
     * -------------------------------------------------------------
     */


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    @Override
    public List<Movie> getMovies(String search) {
        return null;
    }

    /**
     * @return the similarMovies
     */
    @Override
    public List<Movie> getSimilarMovies() {
        return similarMovies;
    }

    /**
     * @param similarMovies the similarMovies to set
     */
    @Override
    public void setSimilarMovies(List<Movie> similarMovies) {
        this.similarMovies = similarMovies;
    }

    /**
     * @return the videos
     */
    @Override
    public List<URL> getVideos() {
        return videos;
    }

    /**
     * @param videos the videos to set
     */
    @Override
    public void setVideos(List<URL> videos) {
        this.videos = videos;
    }

    /**
     * @return the runtime
     */
    @Override
    public int getRuntime() {
        return runtime;
    }

    /**
     * @param runtime the runtime to set
     */
    @Override
    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    /**
     * @return the date
     */
    @Override
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the rating
     */
    @Override
    public int getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    @Override
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * @return the title
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the posterURL
     */
    @Override
    public URL getPosterURL() {
        return posterURL;
    }

    /**
     * @param posterURL the posterURL to set
     */
    @Override
    public void setPosterURL(URL posterURL) {
        this.posterURL = posterURL;
    }

    /**
     * @return the movieImages
     */
    @Override
    public List<URL> getMovieImages() {
        return movieImages;
    }

    /**
     * @param movieImages the movieImages to set
     */
    @Override
    public void setMovieImages(List<URL> movieImages) {
        this.movieImages = movieImages;
    }

    /**
     * @return the lists
     */
    @Override
    public List<List<Movie>> getLists() {
        return lists;
    }

    /**
     * @param lists the lists to set
     */
    @Override
    public void setLists(List<List<Movie>> lists) {
        this.lists = lists;
    }

    /**
     * @return the description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the genres
     */
    @Override
    public List<Genre> getGenres() {
        return genres;
    }

    /**
     * @param genres the genres to set
     */
    @Override
    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    /**
     * @return the backdropUrl
     */
    @Override
    public URL getBackdropUrl() {
        return backdropUrl;
    }

    /**
     * @param backdropUrl the backdropUrl to set
     */
    @Override
    public void setBackdropUrl(URL backdropUrl) {
        this.backdropUrl = backdropUrl;
    }

    /**
     * @return the adult
     */
    @Override
    public boolean isAdult() {
        return adult;
    }

    /**
     * @param adult the adult to set
     */
    @Override
    public void setAdult(boolean adult) {
        this.adult = adult;
    }
}
