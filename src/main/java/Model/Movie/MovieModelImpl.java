package Model.Movie;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import Model.MasterModel;
import Model.Genre.Genre;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * An implementation of MovieModelImpl
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-19
 */
public class MovieModelImpl extends MasterModel implements MovieModel {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */


    /*
     * ---------------------------------------- Constants -----------------------------------------------------------*/

    /*
     * ---------------------------------------- Constructors --------------------------------------------------------*/
    /*
     * ---------------------------------------- Methods -------------------------------------------------------------*/

    @Override
    public List<Movie> getSimilarMovies() {
        throw new  NotImplementedException();
    }

    @Override
    public void setSimilarMovies(List<Movie> similarMovies) {
        throw new  NotImplementedException();
    }

    @Override
    public List<Movie> getMovies(String search) {
        throw new  NotImplementedException();
    }

    @Override
    public Optional<Movie> getMovie(long id) {
        throw new  NotImplementedException();
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */


}

