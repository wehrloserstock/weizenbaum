package Model.Movie;

import Model.Genre.Genre;
import Model.MasterModel;

import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

/**
 * An implementation of MovieModel
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-19
 */
public interface MovieModel {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * @return the similarMovies
     */
    public List<Movie> getSimilarMovies();

    /**
     * @param similarMovies the similarMovies to set
     */
    public void setSimilarMovies(List<Movie> similarMovies);


    /**
     * gets all movies that fit the search
     * @param search
     * @return List
     */
    List<Movie> getMovies(String search);

    /**
     * gets the movie with the current id
     * @param id
     * @return Movie
     */
    Optional<Movie> getMovie(long id);



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

