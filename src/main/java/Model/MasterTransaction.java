package Model;

/**
 * An implementation of Model.MasterTransaction
 * in weizenbaum-moviedatabase
 *
 * @author ytatar
 * @version 1.0
 * @since 2019-Jan-17
 */
public interface MasterTransaction {

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    /* ---------------------------------------- Constants ----------------------------------------------------------- */

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * method for doing database action within a transaction
     */
    void doInTransaction();

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

