package Mock;

import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.LinkedList;

/**
 * An implementation of MockMasterTransaction
 * in weizenbaum-moviedatabase
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-19
 */
public class MockMasterTransaction {

    private Stage stage;
    private StackPane primaryStackPane;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public MockMasterTransaction(Stage stage) {
        this.stage = stage;
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public Stage getStage() {
        return stage;
    }

    public StackPane getPrimaryStackPane() {
        return primaryStackPane;
    }

    public LinkedList<String> getPopularMovies(){
        LinkedList<String> result = new LinkedList<>();
        result.add("graphics/poster.jpg");
        result.add("graphics/poster2.jpg");
        result.add("graphics/poster3.jpg");
        result.add("graphics/poster4.jpg");
        return result;
    }
}
