package Mock;

import Model.Genre.Genre;
import Model.Movie.Movie;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.List;

/**
 * An implementation of MockMovie
 * in weizenbaum-moviedatabase
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-27
 */
public class MockMovie implements Model.Movie.Movie {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    private URL posterURL;
    private URL backDropURL;

    private String title;
    private String rating;
    private int runtime;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public MockMovie(){
        try{
            this.posterURL = Paths.get("graphics/poster.jpg").toUri().toURL();
            this.backDropURL = Paths.get("graphics/backdrop.jpg").toUri().toURL();
        }
        catch (IOException e){
            System.out.println(e);
        }
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    @Override
    public List<Movie> getMovies(String search) {
        return null;
    }

    @Override
    public List<Movie> getSimilarMovies() {
        return null;
    }

    @Override
    public void setSimilarMovies(List<Movie> similarMovies) {

    }

    @Override
    public List<URL> getVideos() {
        return null;
    }

    @Override
    public void setVideos(List<URL> videos) {

    }

    @Override
    public int getRuntime() {
        return 120;
    }

    @Override
    public void setRuntime(int runtime) {

    }

    @Override
    public Date getDate() {
        return new Date(2002,1,1);
    }

    @Override
    public void setDate(Date date) {

    }

    @Override
    public int getRating() {
        return 85;
    }

    @Override
    public void setRating(int rating) {

    }

    @Override
    public String getTitle() {
        return "Harry Potter und der Stein der Weisen";
    }

    @Override
    public void setTitle(String title) {

    }

    @Override
    public URL getPosterURL() {
        return this.posterURL;
    }

    @Override
    public void setPosterURL(URL posterURL) {

    }

    @Override
    public List<URL> getMovieImages() {
        return null;
    }

    @Override
    public void setMovieImages(List<URL> movieImages) {

    }

    @Override
    public List<List<Movie>> getLists() {
        return null;
    }

    @Override
    public void setLists(List<List<Movie>> lists) {

    }

    @Override
    public String getDescription() {
        return "An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself," +
                " his family and the terrible evil that haunts the magical world.";
    }

    @Override
    public void setDescription(String description) {

    }

    @Override
    public List<Genre> getGenres() {
        return null;
    }

    @Override
    public void setGenres(List<Genre> genres) {

    }

    @Override
    public URL getBackdropUrl() {
        return this.backDropURL;
    }

    @Override
    public void setBackdropUrl(URL backdropUrl) {

    }

    @Override
    public boolean isAdult() {
        return true;
    }

    @Override
    public void setAdult(boolean adult) {

    }
}
