package View.Constructs;

import Controller.ScreensManager;
import Model.Movie.Movie;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * An implementation of MovieItemConstruct
 * in weizenbaum-moviedatabase
 *
 * This view is used in the homeScreen to display
 * the movies in a Grid. It contains
 * information about the movie like
 * the release date, poster and rating.
 *
 * This class is an FXRootConstruct,
 * because the view is added multiple times to the
 * homeScreen.
 * Therefor it is necessary to add this view multiple
 * times to the scene graph. This wouldn't be possible
 * with a normal view. For more information
 * see the explanation in the producer classes.
 *
 * @see Controller.Producer.MovieItemProducer
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-27
 */
public class MovieItemConstruct extends AnchorPane{

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    @FXML
    private ImageView imgPoster;

    @FXML
    private Label lblTitel;

    @FXML
    private Label lblJahr;

    @FXML
    private Label lblWertung;

    ScreensManager screensManager;

    Movie movie;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Constructor uses setRoot and setController to this, because this class is an so called FX-Root Construct.
     * That means it isn't only a controller, but an AnchorPane itself. This is useful if the item is displayed
     * multiple times.
     *
     * TODO finish add Paremeters of a movie to setup the view.
     */
    public MovieItemConstruct(Movie movie, ScreensManager screensManager) {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/movieItem.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.screensManager = screensManager;
        this.movie = movie;

        //todo fix url to imageview problem
        //this.imgPoster.setImage(new Image(movie.getPosterURL().getFile()));
        this.imgPoster.setImage(new Image("graphics/poster2.jpg"));
        this.lblTitel.setText(movie.getTitle());
        this.lblJahr.setText(movie.getDate().toString());
        this.lblWertung.setText(Integer.toString(movie.getRating()));
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Uses the screensmanager to display detailed information about the movie.
     * This method is called by the FXML-File, when the MovieItemConstruct is clicked.
     * This is an alternative approach to using an onMouseClickedListener explicit in java code,
     * which is needed here, because an local or anonymous class wouldn't allow to access the
     * non final screensmanager attribute.
     */

    @FXML
    public void goToDetails(){
        screensManager.putMovieDetailsOnStack(this.movie);
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public ImageView getImgPoster() {
        return imgPoster;
    }

    public Label getLblTitel() {
        return lblTitel;
    }

    public Label getLblJahr() {
        return lblJahr;
    }

    public Label getLblWertung() {
        return lblWertung;
    }

    public void setImgPoster(ImageView imgPoster) {
        this.imgPoster = imgPoster;
    }

    public void setLblTitel(Label lblTitel) {
        this.lblTitel = lblTitel;
    }

    public void setLblJahr(Label lblJahr) {
        this.lblJahr = lblJahr;
    }

    public void setLblWertung(Label lblWertung) {
        this.lblWertung = lblWertung;
    }
}
