package View.Constructs;

import Controller.Screens;
import Controller.ScreensManager;
import Model.Movie.Movie;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.IOException;

/**
 * An implementation of MovieDetailsConstruct
 * in weizenbaum-moviedatabase
 *
 * This view is displayed, when the user clicks on
 * an item in the main view, or on a recommended
 * movie. It displays information like the plot
 * more pictures, actors and ratings.
 * From here the user is able to add the movie
 * to a personal list.
 *
 * This class is an FXRootConstruct,
 * because the user is able to click on recommended
 * movies and go to the MovieDetailsConstruct
 * from the chosen movie and vice versa from there on.
 * Therefor it is neccesary to add this view multiple
 * times to the scene graph. This wouldn't be possible
 * with a normal view. For more information
 * see the explanation in the producer classes.
 *
 * @see Controller.Producer.MovieDetailsProducer
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-26
 */
public class MovieDetailsConstruct extends SplitPane {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    @FXML
    private ImageView imgPoster;

    @FXML
    private Label lblTitel;

    @FXML
    private Label lblJahr;

    @FXML
    private Label lblSummary;

    @FXML
    private Label lblWertung;

    @FXML
    private HBox hboxSimmilarMovies;

    @FXML
    private ImageView imgBackdrop;

    private ScreensManager screensManager;
    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Sets up a new MovieDetailsConstruct. To add the needed functionality it
     * is necessary for it to have access to the movie and screenManager.
     *
     * @param movie             movie that is displayed by the view
     * @param screensManager    instance to realise screenSwitching functionality
     */

    public MovieDetailsConstruct(Movie movie, ScreensManager screensManager) {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/movieDetails.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.screensManager = screensManager;

        //this.imgPoster.setImage(new Image(movie.getPosterURL().getFile()));
        this.imgPoster.setImage(new Image("graphics/poster2.jpg"));
        this.lblTitel.setText(movie.getTitle());
        this.lblJahr.setText(movie.getDate().toString());
        this.lblWertung.setText(Integer.toString(movie.getRating()));
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Drops this view from the main StackPane
     */

    @FXML
    public void goBack(){
        screensManager.dropFromStack();
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public ImageView getImgPoster() {
        return imgPoster;
    }

    public Label getLblTitel() {
        return lblTitel;
    }

    public Label getLblJahr() {
        return lblJahr;
    }

    public Label getLblSummary() {
        return lblSummary;
    }

    public Label getLblWertung() {
        return lblWertung;
    }

    public HBox getHboxSimmilarMovies() {
        return hboxSimmilarMovies;
    }

    public ImageView getImgBackdrop() {
        return imgBackdrop;
    }
}
