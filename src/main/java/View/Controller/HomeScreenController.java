package View.Controller;

import Mock.MockMovie;
import View.Constructs.MovieItemConstruct;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;

import java.util.LinkedList;

/**
 * An implementation of HomeScreenController
 * in weizenbaum-moviedatabase
 *
 * JavaFXController of the homeScreen.fxml
 * Getter are used to access the nodes
 * in the HomeScreenVC.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-22
 */
public class HomeScreenController {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    @FXML
    private TilePane tilePanePopular;

    @FXML
    private TilePane tilePaneLatest;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    public void setContentPopularTab(LinkedList<String> list){
        list.stream().forEach(e -> tilePanePopular.getChildren().add(new ImageView(new Image(e))));
    }

    public void setContentLatestTab(LinkedList<MovieItemConstruct> movieItemConstructs){
        movieItemConstructs.stream().forEach(e -> tilePaneLatest.getChildren().add(e));
    }


    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public TilePane getTilePanePopular() {
        return tilePanePopular;
    }

    public TilePane getTilePaneLatest() {
        return tilePaneLatest;
    }
}
