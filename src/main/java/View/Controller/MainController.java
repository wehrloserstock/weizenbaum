package View.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * An implementation of MainVC
 * in weizenbaum-moviedatabase
 *
 *
 * JavaFXController of the main.fxml
 * Getter are used to access nodes in the MainVC
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-20
 */
public class MainController implements Initializable {
    @FXML
    private JFXHamburger hamburger;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private JFXButton searchButton;

    @FXML
    private StackPane stackPane;

    @FXML
    private ImageView imgLogo;

    private DrawerController drawerController;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Is called right at the moment, when the view is loaded and displayed.
     * Right now is used to set up the drawer and hamburger button.
     * @param location
     * @param resources
     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/drawer.fxml"));
        try{
            VBox box = loader.load();
            this.drawerController = loader.getController();
            drawer.setSidePane(box);
        }
        catch (IOException e){
            System.out.println(e);
        }

        HamburgerSlideCloseTransition task = new HamburgerSlideCloseTransition(hamburger);
        task.setRate(-1);
        hamburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            drawer.toggle();
        });
        drawer.setOnDrawerOpening((event) -> {
            task.setRate(task.getRate() * -1);
            task.play();
            drawer.toFront();
        });
        drawer.setOnDrawerClosed((event) -> {
            drawer.toBack();
            task.setRate(task.getRate() * -1);
            task.play();
        });

    }



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public JFXHamburger getHamburger() {
        return this.hamburger;
    }

    public JFXDrawer getDrawer() {
        return this.drawer;
    }

    public JFXButton getSearchButton() {
        return this.searchButton;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public DrawerController getDrawerController() {
        return this.drawerController;
    }

    public ImageView getImgLogo() {
        return imgLogo;
    }
}
