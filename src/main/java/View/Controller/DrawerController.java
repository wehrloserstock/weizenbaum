package View.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;

/**
 * An implementation of DrawerController
 * in weizenbaum-moviedatabase
 *
 * JavaFXController of the drawer.fxml
 * Getter are used to access the nodes in
 * the corresponding VController.
 * In this case the MainVC.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-22
 */
public class DrawerController {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    @FXML
    private JFXButton btnHome;

    @FXML
    private JFXButton btnStats;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public JFXButton getBtnHome() {
        return btnHome;
    }

    public JFXButton getBtnStats() {
        return btnStats;
    }
}
