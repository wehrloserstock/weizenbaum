package Controller;

import Controller.VC.MainVC;
import Mock.MockMasterTransaction;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * An implementation of StartController
 * in MovieDatabase
 *
 * This is the start method of our project.
 * Here the ScreensManager get's
 * the model and the ScreensManager initialised.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-14
 */
public class StartController extends Application {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */

    /**
     * Initalises model and ScreensManger.
     * Sets up Scene and sets it to the primaryStage.
     *
     *
     * @param primaryStage
     * @throws Exception
     */
    public void start(Stage primaryStage) throws Exception {

        //initialise Model
        MockMasterTransaction model = new MockMasterTransaction(primaryStage);

        //initialise ScreensManager
        ScreensManager screensManager = new ScreensManager(model);

        //setup Stage with primary StackPane
        primaryStage.setScene(new Scene(screensManager.getMainView()));

        primaryStage.show();
    }

    /* ---------------------------------------- Attributes ---------------------------------------------------------- */


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */



    /* ---------------------------------------- Methods ------------------------------------------------------------- */



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}

