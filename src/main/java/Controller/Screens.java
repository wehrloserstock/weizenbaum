package Controller;

/**
 * Enum that contains all available
 * views to get to, using the screenManager.
 */

public enum Screens {
    HOMESCREEN,
    STATS,
    LISTS,
    SETTINGS
}
