package Controller.Producer;

import Controller.Screens;
import Controller.ScreensManager;
import Mock.MockMasterTransaction;
import Model.Movie.Movie;
import View.Constructs.MovieDetailsConstruct;
import View.Constructs.MovieItemConstruct;

/**
 * An implementation of MovieItemProducer
 * in weizenbaum-moviedatabase
 *
 * A producer, produces new views and isn't itself a view.
 * That's owing to the fact that there are two kinds of views.
 * Normal views and FXRootConstructs. The difference is only slightly
 * but significant for the later use.
 * FXRootConstructs are itself  Layoutmanagers.
 * The MovieDetailsConstruct for example is a Splitpane.
 *
 * The main advantage and the use in this project is
 * in producing these views. Where normal Views are loaded
 * from the FXMLloader and cannot be instantiated through
 * their controllers, FXRootConstructs can be.
 * For example it would be a fatal error to
 * run this code: DrawerController controller = new Controller.
 * But MovieItemConstruct mdc = new MovieDetailsConstruct,
 * would be alright.
 * The MovieDetailsProducer therefor isn't necessary but only
 * good practice.
 *
 * FXRootConstructs should only be used when needed.
 * In our project we use them to add a view multiple
 * times to Layoutmanager. This wouldn't be possible
 * with the elementary approach through an FXMLloader.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-27
 */
public class MovieItemProducer {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    //model
    private MockMasterTransaction model;

    //Screens manager
    private ScreensManager screensManager;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public MovieItemProducer(MockMasterTransaction model, ScreensManager screensManager) {
        this.model = model;
        this.screensManager = screensManager;
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Produces a new MovieItem, using MovieItemConstruct
     * @return a MovieItem
     *
     */
    public MovieItemConstruct getMovieItem(Movie movie){
        return new MovieItemConstruct(movie,screensManager);
    }



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

}
