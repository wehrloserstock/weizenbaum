package Controller.Producer;

import Controller.ScreensManager;
import Controller.VC.VController;
import Mock.MockMasterTransaction;
import Model.Movie.Movie;
import View.Constructs.MovieDetailsConstruct;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * An implementation of MovieDetailsProducer
 * in weizenbaum-moviedatabase
 *
 * A producer, produces new views and isn't itself a view.
 * That's owing to the fact that there are two kinds of views.
 * Normal views and FXRootConstructs. The difference is only slightly
 * but significant for the later use.
 * FXRootConstructs are itself  Layoutmanagers.
 * The MovieDetailsConstruct for example is a Splitpane.
 *
 * The main advantage and the use in this project is
 * in producing these views. Where normal Views are loaded
 * from the FXMLloader and cannot be instantiated through
 * their controllers, FXRootConstructs can be.
 * For example it would be a fatal error to
 * run this code: DrawerController controller = new Controller.
 * But MovieDetailsConstruct mdc = new MovieDetailsConstruct,
 * would be alright.
 * The MovieDetailsProducer therefor isn't necessary but only
 * good practice.
 *
 * FXRootConstructs should only be used when needed.
 * In our project we use them to add a view multiple
 * times to Layoutmanager. This wouldn't be possible
 * with the elementary approach through an FXMLloader.
 *
 * @author Nicolas
 * @version 3.0
 * @since 2019-Jan-27
 */
public class MovieDetailsProducer{

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */

    //model
    private MockMasterTransaction model;
    //screensmanager
    private ScreensManager screensManager;


    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Constructor initialises the model and screenManager for later access through the
     * Movie Details Construct.
     * @param model
     * @param screensManager    used to change screens
     */
    public MovieDetailsProducer(MockMasterTransaction model, ScreensManager screensManager)
    {
        this.model = model;
        this.screensManager = screensManager;
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * Produces a new MovieDetailsConstruct with access to the model and the screenManager instance.
     * @param movie
     * @return  MovieDetailsConstruct instance
     */
    public MovieDetailsConstruct getMovieDetailsConstruct(Movie movie){
        return new MovieDetailsConstruct(movie, screensManager);
    }
}
