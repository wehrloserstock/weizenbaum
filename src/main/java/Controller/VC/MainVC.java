package Controller.VC;

import Controller.Screens;
import Controller.ScreensManager;
import Mock.MockMasterTransaction;
import View.Controller.MainController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

import java.io.IOException;

/**
 * An implementation of MainVC
 * in weizenbaum-moviedatabase
 *
 * Controller Class for the MainController.
 * This class fills the views from the
 * main.fxml and adds logic to them.
 *
 * The main Part of the App is the top bar,
 * as well as the drawer on the left.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-22
 */
public class MainVC implements VController {

    //model
    private MockMasterTransaction model;
    //view
    private MainController javaFXController;
    //Node
    private Parent mainView;
    //Screens manager
    private ScreensManager screensManager;


    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Initialises the view, model is used to get primary Stage;
     * @param model used to get primary stage
     */

    public MainVC(MockMasterTransaction model, ScreensManager screensManager){
        this.model = model;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/main.fxml"));
        try {
            this.mainView = fxmlLoader.load();
            this.javaFXController = fxmlLoader.getController();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.screensManager = screensManager;

        setUpDrawerEvents();
    }



    /* ---------------------------------------- Methods ------------------------------------------------------------- */


    /**
     * Shows the stats View
     */
    public void showStatsView(){
        screensManager.switchScreenTo(Screens.STATS);
    }

    /**
     * Shows the homeScreenView
     */
    public void showHomeScreen(){
        screensManager.switchScreenTo(Screens.HOMESCREEN);
    }

    /**
     * Sets up the listeners for the buttons in the drawer
     */
    private void setUpDrawerEvents(){
        javaFXController.getDrawerController().getBtnHome().setOnAction(event -> showHomeScreen());
        javaFXController.getDrawerController().getBtnStats().setOnAction(event -> showStatsView());
    }
    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public StackPane getCenterStackPane(){
        return this.javaFXController.getStackPane();
    }

    public MainController getJavaFXController() {
        return this.javaFXController;
    }

    public Parent getView() {
        return this.mainView;
    }
}
