package Controller.VC;

import Controller.ScreensManager;
import Mock.MockMasterTransaction;
import Mock.MockMovie;
import Model.Movie.Movie;
import View.Constructs.MovieItemConstruct;
import View.Controller.HomeScreenController;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * An implementation of HomeScreenVC
 * in weizenbaum-moviedatabase
 *
 * Controller class for the HomeScreenController.
 * The homeScreen.fxml is loaded here
 * and views are filled with information here.
 *
 * The homeScreen is the center part of the application,
 * that contains the most popular movies and latest movies
 * in a grid like view.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-19
 */
public class HomeScreenVC implements VController{

    //model
    private MockMasterTransaction model;
    //view
    private HomeScreenController javaFXController;
    //Node
    private Parent homeScreenView;
    //Screens manager
    private ScreensManager screensManager;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Initialises the view, model is used to get primary Stage;
     * @param model used to get primary stage
     */

    public HomeScreenVC(MockMasterTransaction model, ScreensManager screensManager){
        this.model = model;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/homeScreen.fxml"));
        try {
            this.homeScreenView = fxmlLoader.load();
            this.javaFXController = fxmlLoader.getController();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.screensManager = screensManager;

        setContentPopularTab();
        setContentLatestTab();
    }


    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Sets the content of the Popular Tab
     * with data from the model
     */
    public void setContentPopularTab(){
        javaFXController.setContentPopularTab(model.getPopularMovies());
    }

    /**
     * Sets the content of the Latest Tab
     * with data from the model
     */
    public void setContentLatestTab(){
        //model with views
        LinkedList<Movie> movies = new LinkedList<>();
        movies.add(new MockMovie());
        movies.add(new MockMovie());

        javaFXController.setContentLatestTab(movieToMovieItemConstruct(movies));
    }

    /**
     * Converts the Modal.Movie Objects in View.MovieItemConstruct Objects
     * which then can be displayed by the HomeScreen
     * @param movies List of Movies which are to be converted.
     * @return
     */
    public LinkedList<MovieItemConstruct> movieToMovieItemConstruct(List<Movie> movies){
        LinkedList<MovieItemConstruct> movieItemConstructs = new LinkedList<>();
        movies.stream().forEach( e -> movieItemConstructs.add(new MovieItemConstruct(e,screensManager)));
        return movieItemConstructs;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public HomeScreenController getJavaFXController() {
        return this.javaFXController;
    }

    public Parent getView() {
        return this.homeScreenView;
    }
}
