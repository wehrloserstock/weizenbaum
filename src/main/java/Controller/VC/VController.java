package Controller.VC;

import Controller.ScreensManager;
import javafx.scene.Parent;

public interface VController {
    Parent getView();
}
