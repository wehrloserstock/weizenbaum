package Controller.VC;

import Controller.ScreensManager;
import Mock.MockMasterTransaction;
import View.Controller.StatsController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * An implementation of StatsVC
 * in weizenbaum-moviedatabase
 *
 * Controller Class for the StatsController
 * The elements in the stats.fxml are
 * filled here and logic is added to them here.
 *
 * The Stats view contains stats about the users lists
 * and viewing habits.
 *
 * TODO update with real features later
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-22
 */
public class StatsVC implements VController{

    //model
    private MockMasterTransaction model;
    //view
    private StatsController javaFXController;
    //Node that is the homeScreen
    private Parent statsView;
    //Screens manager
    private ScreensManager screensManager;

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */



    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    public StatsVC(MockMasterTransaction model, ScreensManager screensManager) {
        this.model = model;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/stats.fxml"));
        try {
            this.statsView = fxmlLoader.load();
            this.javaFXController = fxmlLoader.getController();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */



    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    public StatsController getJavaFXController() {
        return this.javaFXController;
    }

    public Parent getView() {
        return this.statsView;
    }
}
