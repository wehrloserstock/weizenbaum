package Controller;

import Controller.Producer.MovieDetailsProducer;
import Controller.Producer.MovieItemProducer;
import Controller.VC.*;
import Mock.MockMasterTransaction;
import Model.Movie.Movie;
import View.Constructs.MovieItemConstruct;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

/**
 * An implementation of ScreensManager
 * in weizenbaum-moviedatabase
 *
 * Singleton-Pattern implementation
 * for a GUI-Switcher.
 *
 * The ScreensManager knows all views and is able
 * to switch the screens.
 * The Main View is always kept visible and the
 * center stackpane of the mainView switches its
 * content based on the ScreensManager.
 *
 * There are two kinds of views to switch here.
 * First the normal views, as declared in the
 * Screens Enum.
 *
 * Secondly MovieDetailsConstruct and the
 * MovieItemConstruct, to which cannot simply
 * be switched. They are called explicit
 * with content added to them first.
 * The ScreensManager contains
 * also the methods for them.
 *
 * @author Nicolas
 * @version 1.0
 * @since 2019-Jan-26
 */
public class ScreensManager {

    /* ---------------------------------------- Main ---------------------------------------------------------------- */



    /* ---------------------------------------- Attributes ---------------------------------------------------------- */
    //all managed VCs
    private MainVC mainVC;
    private HomeScreenVC homeScreenVC;
    private StatsVC statsVC;

    //all producers
    private MovieItemProducer movieItemProducer;
    private MovieDetailsProducer movieDetailsProducer;

    //model
    private MockMasterTransaction model;

    //center StackPane, which will switch its content
    //based on menu (Stats, HomeScree, Lists ...).
    private StackPane stackPane;

    //counts how many elements are on the stack
    private int stackCount;

    /* ---------------------------------------- Constants ----------------------------------------------------------- */



    /* ---------------------------------------- Constructors -------------------------------------------------------- */

    /**
     * Initialises the ScreensManager with the model,
     * which gets added to all the controlled VControllers.
     *
     * @param model
     */
    public ScreensManager(MockMasterTransaction model) {
        //model thats given by the start method
        this.model = model;

        //initialise producers
        movieItemProducer = new MovieItemProducer(model,this);
        movieDetailsProducer = new MovieDetailsProducer(model,this);

        //initialise managed VCs
        this.mainVC = new MainVC(model,this);

        this.homeScreenVC = new HomeScreenVC(model,this);

        this.statsVC = new StatsVC(model,this);

        //initialise center StackPane
        this.stackPane = mainVC.getCenterStackPane();

        //initialise stackCount
        stackCount = 0;

        switchScreenTo(Screens.HOMESCREEN);
    }

    /* ---------------------------------------- Methods ------------------------------------------------------------- */

    /**
     * Switches Screen To chosen Screen determined by the Enum Screens
     * Isn't able to switch to the MovieItemConstruct or the MovieDetailsConstruct-
     *
     * @param screens   Enum containg names of all available screens
     */
    public void switchScreenTo(Screens screens){
        switch (screens){
            case HOMESCREEN:
                this.stackPane.getChildren().clear();
                this.stackPane.getChildren().add(homeScreenVC.getView());
                break;
            case STATS:
                this.stackPane.getChildren().clear();
                this.stackPane.getChildren().add(statsVC.getView());
                break;
        }
    }


    /**
     * Puts any chosen view on the center Stackpane
     * in the Main View.
     *
     * @param vController
     */
    public void putOnStack(VController vController){
        stackCount++;
        stackPane.getChildren().add(vController.getView());
    }

    /**
     * Puts a MovieDetailsConstruct on the stack.
     * @param movie used to fill the view with information.
     */
    public void putMovieDetailsOnStack(Movie movie){
        stackCount++;
        stackPane.getChildren().add(movieDetailsProducer.getMovieDetailsConstruct(movie));
    }

    /**
     * Drops the top view from the stack.
     * @return  if the return value isn't null the drop was successful
     */
    public Node dropFromStack(){
        Node removedItem = null;

        if(this.stackCount > 0){
            removedItem = stackPane.getChildren().remove(stackPane.getChildren().size()-1);
        }

        return removedItem;
    }

    /* ---------------------------------------- S/Getters ----------------------------------------------------------- */

    /**
     * Returns the main view.
     * Used in the start method to fill the scene.
     * @return  Main View, which contains the center Stackpane, thats
     *          used to display all other views.
     */
    public Parent getMainView(){
        return mainVC.getView();
    }
}
