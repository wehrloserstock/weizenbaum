[![pipeline status](https://gitlab.com/wehrloserstock/weizenbaum/badges/develop/pipeline.svg)](https://gitlab.com/wehrloserstock/weizenbaum/commits/develop)

**Git commit message Guidelines**
    
    - Sprache: Englisch
    - Zeitform: Präsenz
    - Form: Deskriptiv gegenüber Code Änderung, nicht Datei oder Pfad
    - Unbedingt: Sinnvolle Änderung. KEIN "Änderung 2" | "aaa" | "test123" | "rückgängig", etc.

**Git commit guidelines**

    - Ein commit pro Änderung. Keine "Riesen-Commits", die mehrere Datei Änderungen in einem in sich vereinen. So viel und so oft commiten wie möglich.
    - Beachtung der Git Modell Orientierung
    - Ab Version 0.1(Projektskellet steht): Kein direkter commit und push auf develop möglich, nur noch durch Merge Reuqests von subbranches.
  
**Git merge request guidelines**

    - KEIN "squash-commits"
    - "remove source branch" wenn möglich.

**Coding Guidelines**
    
    Beachtung des UML Diagrams.

    Möglichst nahe Orientierung an den [Google Java coding Guidelines](https://google.github.io/styleguide/javaguide.html)
    

**Git Modell Orientierung**

![](https://nvie.com/img/git-model@2x.png)

Hilfe:

Klonen des Projektes per:

`git clone adress`

Fetching aller Daten und Branches von Remote auf das local Repository (Updaten der Remote tracking branches, ändert nichts an den existierenden Dateien des Working Directory, fügt aber neue hinzu. `git merge` geschieht manuell`)

`git fetch --all`

Pullen aller Daten und Branches von Remote auf das Working Directory (Tut zuerst `git fetch`, dann `git merge`. Ändert das Working Directory)

`git pull --all`

Erstellen eines neuen branches:

`git checkout -b 'branchname'`

Wechseln vom aktuellen branch auf einen anderen:

`git branch #listet alle branches auf`

`git checkout branchname`

Wechseln auf einen Branch von Origin und tracken diesem:

`git checkout -b branch origin/branch` (implizit `git checkout -b --track branch origin/branch`)

Herauspicken einer einzelnen Datei/Pfad von einem branch zu einem anderen:

 `git checkout <source_branch> -- <path>
 `
Hinzufügen einer neuen Datei zu git:

`git add dateiname`

Änderungen an einer Datei speichern:

`git stage dateiname`

`git commit -m 'kommentar'`

`git push`

Anzeigen aller Änderungen im Dateisystem:

`git status`

Anzeigen ALLER branches, lokal und remote
`git branch -av`

Aneigen der remote urls:
`git remote -v`

VORSICHT:
 - Tote Branches löschen und alle lokalen branches mit origin aktualisieren:
 -  `git remote prune origin`
 - Lokale branch löschen:
 -  `git branch -d branchname`

